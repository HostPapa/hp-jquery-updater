# HP jQueryUI Updater

This plugin updates vulnerable jQueryUI version in WordPress instances.

## Prerequisites

WordPress += 4.x

## Installing

Add the following to `composer.json`:

```json
{
  "repositories": 
  {
    "type": "vcs",
    "url": "https://bitbucket.org/HostPapa/hp-jquery-updater"
  },
  "require": {
    "hostpapa/hp-jquery-updater": "^1.0"
  }
}
```

To install, run:

```
$ composer install
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/HostPapa/hp-jquery-updater/downloads/?tab=tags).

## Next Steps

Initial work intends to fix PCI compliance errors due to jQueryUI v1.11.4, however, the plugin should add another callback function using `wp-load` hook (or something in between `init` hook and `wp_enqueue_scripts` hook) to register files and avoid enqueuing them, being careful to not touch Administration panel.
Current scripts will enqueue files to avoid missing dependencies, even a couple that are not strictly necessary, this should be improved. A combined file mechanism could be implemented using PHP's output buffer capabilities, to avoid multiple content requests.

## Authors

* **Tony Dinarte** - *Initial development*
