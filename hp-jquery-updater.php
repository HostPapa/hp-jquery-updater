<?php
/*
 * Plugin Name: HostPapa jQueryUI Updater
 * Description: This plugin updates jQueryUI to the latest  stable version.
 * Version: 1.0
 * Author: Tony Dinarte
 * Author URI: https://hostpapa.com
 * Text Domain: hostpapa-jquery-ui-updater
 */

/*
 * Replace jQueryUI with a newer version
 * @version 1.0
 * @since 1.0
 */
function hostpapa_jquery_ui_updater()
{
    $coreIsEnqueued = wp_script_is('jquery-ui-core', 'enqueued');
    $coreIsRegistered = wp_script_is('jquery-ui-core', 'registered');

    // Guard clause, If jQuery core is not needed then no updates are necessary.
    if ($coreIsEnqueued || $coreIsRegistered) {
        $mouseWidgetIsEnqueued = wp_script_is('jquery-ui-mouse', 'enqueued');
        $mouseWidgetIsRegistered = wp_script_is('jquery-ui-mouse', 'registered');
        $effectsCoreIsEnqueued = wp_script_is('jquery-effects-core', 'enqueued');
        $effectsCoreIsRegistered = wp_script_is('jquery-effects-core', 'registered');
        $widgetCoreIsEnqueued = wp_script_is('jquery-ui-widget', 'enqueued');
        $widgetCoreIsRegistered = wp_script_is('jquery-ui-widget', 'registered');

        wp_deregister_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-core', plugins_url('/js/jquery-ui-core.min.js', __FILE__), false, '1.12.1');

        if (wp_script_is('jquery-ui-draggable', 'enqueued') && ($mouseWidgetIsEnqueued || $mouseWidgetIsRegistered)) {
            wp_deregister_script('jquery-ui-draggable');
            wp_enqueue_script('jquery-ui-draggable', plugins_url('/js/jquery-ui-draggable.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-droppable', 'enqueued') && ($mouseWidgetIsEnqueued || $mouseWidgetIsRegistered)) {
            wp_deregister_script('jquery-ui-droppable');
            wp_enqueue_script('jquery-ui-droppable', plugins_url('/js/jquery-ui-droppable.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-resizable', 'enqueued') && ($mouseWidgetIsEnqueued || $mouseWidgetIsRegistered)) {
            wp_deregister_script('jquery-ui-resizable');
            wp_enqueue_script('jquery-ui-resizable', plugins_url('/js/jquery-ui-resizable.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-selectable', 'enqueued') && ($mouseWidgetIsEnqueued || $mouseWidgetIsRegistered)) {
            wp_deregister_script('jquery-ui-selectable');
            wp_enqueue_script('jquery-ui-selectable', plugins_url('/js/jquery-ui-selectable.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-sortable', 'enqueued') && ($mouseWidgetIsEnqueued || $mouseWidgetIsRegistered)) {
            wp_deregister_script('jquery-ui-sortable');
            wp_enqueue_script('jquery-ui-sortable', plugins_url('/js/jquery-ui-sortable.min.js', __FILE__), false, '1.12.1');
        }
    } else {
        return;
    }

    if ($effectsCoreIsEnqueued || $effectsCoreIsRegistered) {
        wp_deregister_script('jquery-effects-core');
        wp_enqueue_script('jquery-effects-core', plugins_url('/js/jquery-ui-effect.min.js', __FILE__), false, '1.12.1');
    }
    if ($widgetCoreIsEnqueued || $widgetCoreIsRegistered) {
        wp_deregister_script('jquery-ui-widget');
        wp_enqueue_script('jquery-ui-widget', plugins_url('/js/jquery-ui-widget.min.js', __FILE__), false, '1.12.1');
    }
    if (wp_script_is('jquery-ui-position', 'enqueued') || wp_script_is('jquery-ui-position', 'registered')) {
        wp_deregister_script('jquery-ui-position');
        wp_enqueue_script('jquery-ui-position', plugins_url('/js/jquery-ui-position.min.js', __FILE__), false, '1.12.1');
    }


    if ($widgetCoreIsEnqueued || $widgetCoreIsRegistered) {
        if (wp_script_is('jquery-ui-accordion', 'enqueued') || wp_script_is('jquery-ui-accordion', 'registered')) {
            wp_deregister_script('jquery-ui-accordion');
            wp_enqueue_script('jquery-ui-accordion', plugins_url('/js/jquery-ui-widget-accordion.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-autocomplete', 'enqueued') || wp_script_is('jquery-ui-autocomplete', 'registered')) {
            wp_deregister_script('jquery-ui-autocomplete');
            wp_enqueue_script('jquery-ui-autocomplete', plugins_url('/js/jquery-ui-widget-autocomplete.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-button', 'enqueued') || wp_script_is('jquery-ui-button', 'registered')) {
            wp_deregister_script('jquery-ui-button');
            wp_enqueue_script('jquery-ui-button', plugins_url('/js/jquery-ui-widget-button.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-datepicker', 'enqueued') || wp_script_is('jquery-ui-datepicker', 'registered')) {
            wp_deregister_script('jquery-ui-datepicker');
            wp_enqueue_script('jquery-ui-datepicker', plugins_url('/js/jquery-ui-widget-datepicker.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-dialog', 'enqueued') || wp_script_is('jquery-ui-dialog', 'registered')) {
            wp_deregister_script('jquery-ui-dialog');
            wp_enqueue_script('jquery-ui-dialog', plugins_url('/js/jquery-ui-widget-dialog.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-menu', 'enqueued') || wp_script_is('jquery-ui-menu', 'registered')) {
            wp_deregister_script('jquery-ui-menu');
            wp_enqueue_script('jquery-ui-menu', plugins_url('/js/jquery-ui-widget-menu.min.js', __FILE__), false, '1.12.1');
        }

        if ($mouseWidgetIsEnqueued || $mouseWidgetIsRegistered) {
            wp_deregister_script('jquery-ui-mouse');
            wp_enqueue_script('jquery-ui-mouse', plugins_url('/js/jquery-ui-widget-mouse.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-progressbar', 'enqueued') || wp_script_is('jquery-ui-progressbar', 'registered')) {
            wp_deregister_script('jquery-ui-progressbar');
            wp_enqueue_script('jquery-ui-progressbar', plugins_url('/js/jquery-ui-widget-progressbar.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-selectmenu', 'enqueued') || wp_script_is('jquery-ui-selectmenu', 'registered')) {
            wp_deregister_script('jquery-ui-selectmenu');
            wp_enqueue_script('jquery-ui-selectmenu', plugins_url('/js/jquery-ui-widget-selectmenu.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-slider', 'enqueued') || wp_script_is('jquery-ui-slider', 'registered')) {
            wp_deregister_script('jquery-ui-slider');
            wp_enqueue_script('jquery-ui-slider', plugins_url('/js/jquery-ui-widget-slider.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-spinner', 'enqueued') || wp_script_is('jquery-ui-spinner', 'registered')) {
            wp_deregister_script('jquery-ui-spinner');
            wp_enqueue_script('jquery-ui-spinner', plugins_url('/js/jquery-ui-widget-spinner.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-tabs', 'enqueued') || wp_script_is('jquery-ui-tabs', 'registered')) {
            wp_deregister_script('jquery-ui-tabs');
            wp_enqueue_script('jquery-ui-tabs', plugins_url('/js/jquery-ui-widget-tabs.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-ui-tooltip', 'enqueued') || wp_script_is('jquery-ui-tooltip', 'registered')) {
            wp_deregister_script('jquery-ui-tooltip');
            wp_enqueue_script('jquery-ui-tooltip', plugins_url('/js/jquery-ui-widget-tooltip.min.js', __FILE__), false, '1.12.1');
        }
    }


    if ($effectsCoreIsEnqueued) {
        if (wp_script_is('jquery-effects-blind', 'enqueued')) {
            wp_deregister_script('jquery-effects-blind');
            wp_enqueue_script('jquery-effects-blind', plugins_url('/js/jquery-ui-effect-blind.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-bounce', 'enqueued')) {
            wp_deregister_script('jquery-effects-bounce');
            wp_enqueue_script('jquery-effects-bounce', plugins_url('/js/jquery-ui-effect-bounce.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-clip', 'enqueued')) {
            wp_deregister_script('jquery-effects-clip');
            wp_enqueue_script('jquery-effects-clip', plugins_url('/js/jquery-ui-effect-clip.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-drop', 'enqueued')) {
            wp_deregister_script('jquery-effects-drop');
            wp_enqueue_script('jquery-effects-drop', plugins_url('/js/jquery-ui-effect-drop.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-explode', 'enqueued')) {
            wp_deregister_script('jquery-effects-explode');
            wp_enqueue_script('jquery-effects-explode', plugins_url('/js/jquery-ui-effect-explode.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-fade', 'enqueued')) {
            wp_deregister_script('jquery-effects-fade');
            wp_enqueue_script('jquery-effects-fade', plugins_url('/js/jquery-ui-effect-fade.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-fold', 'enqueued')) {
            wp_deregister_script('jquery-effects-fold');
            wp_enqueue_script('jquery-effects-fold', plugins_url('/js/jquery-ui-effect-fold.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-highlight', 'enqueued')) {
            wp_deregister_script('jquery-effects-highlight');
            wp_enqueue_script('jquery-effects-highlight', plugins_url('/js/jquery-ui-effect-highlight.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-puff', 'enqueued')) {
            wp_deregister_script('jquery-effects-puff');
            wp_enqueue_script('jquery-effects-puff', plugins_url('/js/jquery-ui-effect-puff.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-pulsate', 'enqueued')) {
            wp_deregister_script('jquery-effects-pulsate');
            wp_enqueue_script('jquery-effects-pulsate', plugins_url('/js/jquery-ui-effect-pulsate.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-scale', 'enqueued')) {
            wp_deregister_script('jquery-effects-scale');
            wp_enqueue_script('jquery-effects-scale', plugins_url('/js/jquery-ui-effect-scale.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-shake', 'enqueued')) {
            wp_deregister_script('jquery-effects-shake');
            wp_enqueue_script('jquery-effects-shake', plugins_url('/js/jquery-ui-effect-shake.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-size', 'enqueued')) {
            wp_deregister_script('jquery-effects-size');
            wp_enqueue_script('jquery-effects-size', plugins_url('/js/jquery-ui-effect-size.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-slide', 'enqueued')) {
            wp_deregister_script('jquery-effects-slide');
            wp_enqueue_script('jquery-effects-slide', plugins_url('/js/jquery-ui-effect-slide.min.js', __FILE__), false, '1.12.1');
        }

        if (wp_script_is('jquery-effects-transfer', 'enqueued')) {
            wp_deregister_script('jquery-effects-transfer');
            wp_enqueue_script('jquery-effects-transfer', plugins_url('/js/jquery-ui-effect-transfer.min.js', __FILE__), false, '1.12.1');
        }
    }
}

add_action('wp_enqueue_scripts', 'hostpapa_jquery_ui_updater');
